# Containers for PostgreSQL stuff

You will find here a list of interesting curated PG containers mostly based in alpine linux images. Currently available images are:
* psqlclt: a container with a psql client
* pg2parquet: a container that copies pg tables into parquet files
* mariadbclt: a container with a mariadb client
* onequery: a container with a webservice to query PG and s3 files
* pg14ts281patroni: a container with PG14/TS2.8.1 and patroni 2.1.7 or your preferred patroni version
* pg2pa2s3: a container with a script to export from PG to parquet to an S3 bucket
* pg2parquet: a container with ascript to export from PG to parquet
* pg_back: a container with an s3 PG exporter
* psqlclt: a container with a PG client
* qpq-ws: a container with a webservice that can read parquet files
* tscale-exporter: an container with a timescale exporter

