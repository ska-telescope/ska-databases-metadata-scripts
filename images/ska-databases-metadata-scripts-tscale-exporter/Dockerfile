# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3-slim

EXPOSE 8000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
ADD https://gitlab.com/ska-telescope/ska-databases-metadata-scripts/-/raw/main/pg/tscale-exporter/requirements.txt /tmp
ADD https://gitlab.com/ska-telescope/ska-databases-metadata-scripts/-/raw/main/pg/tscale-exporter/queries/all_queries.sql /tmp
ADD https://gitlab.com/ska-telescope/ska-databases-metadata-scripts/-/raw/main/pg/tscale-exporter/ts-exporter.py /tmp
COPY config.yaml /tmp
RUN python -m pip install -r /tmp/requirements.txt

WORKDIR /app
RUN mv /tmp/*py /app
RUN mv /tmp/config.yaml /app
RUN mkdir /app/queries
RUN mv /tmp/*sql /app/queries

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

CMD [ "python", "ts-exporter.py" ]

