PROJECT = ska-databases-metadata-scripts
#HELM_RELEASE ?= ska-databases-metadata-scripts

BASE = $(shell pwd)
OCI_ROOT_DIR=images
include .make/base.mk
include .make/oci.mk

-include PrivateRules.mak

ALL_IMAGES=$(patsubst ska-databases-metadata-scripts-%, %, $(OCI_IMAGES))
ALL_BUILD_JOBS=$(addprefix oci-build-,$(ALL_IMAGES))

# Build all the images in order

default: oci-build-all

