'''I do stuff with PostgreSQL'''
import psycopg2

def getconn(config):
    return psycopg2.connect(
            host=config["DB_HOST"],
            database=config["DB_NAME"],
            user=config["DB_USER"],
            password=config["DB_PASSWORD"]
        )

def pgquery(request, config):
    '''I do queries'''
    pgconn = getconn(config)
    cursor = pgconn.cursor()

        # Construct the SQL query
    if request.conf_filter != "":
        query = f"""
            SELECT * FROM {request.table_name}
            WHERE att_conf_id = %s data_time BETWEEN %s AND %s;
        """
        cursor.execute(query, (request.config_filter, request.start_time, request.end_time))
    else:
        query = f"""
            SELECT * FROM {request.table_name}
            WHERE data_time BETWEEN %s AND %s;"""
        cursor.execute(query, (request.start_time, request.end_time)) 

        # Fetch all results
    results = cursor.fetchall()

        # Close the connection
    cursor.close()
    pgconn.close()

    return {"results": results}

def getconf(request, config):
    '''Get ids from config names'''
    pgconn = getconn(config)
    cursor = pgconn.cursor()
    if "%" in request.name:
        query = "SELECT * FROM att_conf where name like %s order by 1;"
    else:
        query = "SELECT * FROM att_conf where name = %s order by 1;"
    cursor.execute(query, (request.name))
    
    results = cursor.fetchall()
    return {"config": results}

def getparam(request, config):
    '''Get ids from config parameters'''
    pgconn = getconn(config)
    cursor = pgconn.cursor()
    if "%" in request.name:
        query = "SELECT * FROM att_parameter where name like %s order by 1;"
    else:
        query = "SELECT * FROM att_parameter where name = %s order by 1;"
    cursor.execute(query, (request.name))
    
    results = cursor.fetchall()
    return {"config": results}