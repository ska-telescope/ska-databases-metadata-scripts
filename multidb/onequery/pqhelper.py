'''I do stuff with parquet'''
import re
import datetime
import boto3
from io import BytesIO
import pandas as pd


def pqconfig(config):
    '''Get an s3 resource from a config'''
    s3_resource = None
    if "S3_BUCKET" in list(config.keys()):
        s3_resource = boto3.resource("s3",
            endpoint_url = config["ENDPOINT_URL"],
            aws_access_key_id = config["ACCESS_KEY"],
            aws_secret_access_key = config["SECRET_KEY"],
            config=boto3.session.Config(signature_version='s3v4'),
            verify=False
        )
    print(s3_resource)
    return s3_resource

def pqquery(request, parquetfiles, config):
    '''I do queries in the parquet files'''
    results = []
    s3_resource = pqconfig(config)
    bucket = s3_resource.Bucket(config["S3_BUCKET"])
    indata = BytesIO()
    for parquetfile in parquetfiles:
        bucket.download_fileobj(parquetfile, indata)
        df = pd.read_parquet(indata)
        if filter == "":
            result_df = df.loc[(df["data_time"] >= request.start_day) & (df["data_time"] < request.end_day)]
        else:
            result_df = df.loc[(df["att_conf_id"] == request.conf_filter) & (df["data_time"] >= request.start_day) & (df["data_time"] < request.end_day)]
        results.append(result_df.to_json(date_format='iso'))
    return {"results": results}

def gets3files(config):
    '''I list s3 files'''

    s3_resource = pqconfig(config)
    bucket = s3_resource.Bucket(config["S3_BUCKET"])
    files = []
    for obj in bucket.objects.all():
        files.append(obj.key)
    return files

def find_matching_files(parquet_files, table_name, start_date, end_date):
    """
    Check if there are Parquet files for a given table and time range.

    Args:
        parquet_files (list): List of Parquet file paths.
        table_name (str): The table name to filter by.
        start_date (str): Start date in YYYYMMDD format.
        end_date (str): End date in YYYYMMDD format.

    Returns:
        list: List of matching Parquet file names.
    """
    FILE_PATTERN = re.compile(r".*/(?P<table>\w+)_(?P<start>\d{4}-\d{2}-\d{2})_(?P<end>\d{4}-\d{2}-\d{2})\.parquet")

    start_dt = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_dt = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    matching_files = []

    for file in parquet_files:
        match = FILE_PATTERN.match(file)
        if match:
            file_table = match.group("table")
            file_start_dt = datetime.datetime.strptime(match.group("start"), "%Y-%m-%d")
            file_end_dt = datetime.datetime.strptime(match.group("end"), "%Y-%m-%d")

            # Check if table matches and if date ranges overlap
            if file_table == table_name and not (file_end_dt < start_dt or file_start_dt > end_dt):
                matching_files.append(file)

    return matching_files
