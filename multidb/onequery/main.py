'''An app to query everything in one place'''
import logging

from fastapi import FastAPI
from dotenv import dotenv_values
from pydantic import BaseModel

from pghelper import pgquery, getconf
from pqhelper import pqquery, gets3files, find_matching_files

config = dotenv_values()

# Initialize FastAPI app
app = FastAPI()

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Initialize S3 client

#All the files on S3, that might be read from the s3 bucket in the future
parquetfiles = []
parquetfiles = gets3files(config)

class QueryRequest(BaseModel):
    '''The query request'''
    table_name: str
    conf_filter: str = ""
    start_day: str
    end_day: str

class ConfigRequest(BaseModel):
    '''query for a config id'''
    name: str

@app.post("/query")
def query_data(request: QueryRequest):
    '''I look at the data'''
    # Validate input
    if request.start_day > request.end_day:
        raise HTTPException(status_code=400, detail="Start time must be before end time.")

    inparquet = []
    # Check if the data is in parquet.
    inparquet = find_matching_files(parquetfiles, request.table_name, request.start_day, request.end_day)

    if len(inparquet)==0:
        # Not in parquet
        return pgquery(request, config)
    else:
        # in one or more parquet files
        return pqquery(request, inparquet, config)


@app.get("/getconf")
def query_config(request: ConfigRequest):
    '''Get a config id'''
    return getconf(request, config)

# Regex pattern to extract table name and dates

@app.get("/gets3files", summary="List files in S3 bucket", description="Retrieves a list of files from an S3 bucket.")
def get_s3_files():
    """
    Fetches all files from the S3 bucket
    
    Returns:
        A list of files.
    """ 
    parquetfiles = []
    parquetfiles = gets3files(config)
    return parquetfiles

