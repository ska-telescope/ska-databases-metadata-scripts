# ska-databases-metadata-scripts

These are a collection of scripts for database operations. You will find scripts for:

* [MariaDB](mariadb)
* [PostgreSQL](pg)
* [Elastic search](elastic)
* [Ansible playbooks](ansible)
* [Replication tester](repTester)
* [Vault password synchronization](changeSecrets)

