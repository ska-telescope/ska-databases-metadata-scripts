"""
FastAPI web service for querying Parquet files from S3 or local filesystem.

This service provides an endpoint to query Parquet files using pandas' query function.
It supports both local file paths and Amazon S3 paths.
"""

import os
import io
import logging
import boto3

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel, Field
import pandas as pd
from dotenv import dotenv_values
from io import BytesIO

# Load environment variables
config = dotenv_values()

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

app = FastAPI()

s3_resource = None
local_prefix = "data/" #prefix for local files
postfix = ".parquet"

# AWS S3 Configuration (optional)
if "S3_BUCKET" in list(config.keys()):
    s3_resource = boto3.resource("s3",
        endpoint_url = config["ENDPOINT_URL"],
        aws_access_key_id = config["ACCESS_KEY"],
        aws_secret_access_key = config["SECRET_KEY"],
        config=boto3.session.Config(signature_version='s3v4'),
        verify=False
    )
    print(config["ACCESS_KEY"])
    logger.info("S3 configured")
else:
    logger.info("Local file configured")


class QueryRequest(BaseModel):
    """
    Request model for querying a Parquet file.
    
    Attributes:
        file_path (str): The path to the Parquet file (either local or S3 URI).
        query (str): The pandas-compatible query string to filter the data.
    """
    file_path: str  # Can be local path or s3://bucket-name/path/to/file.parquet
    query: str  = Field(default="index >= 0")# pandas-compatible query string

@app.post("/query")
def query_parquet(data: QueryRequest):
    """
    Endpoint to query a Parquet file and return the results as JSON.
    
    Args:
        data (QueryRequest): The request payload containing file_path and query string.
    
    Returns:
        List[Dict]: The filtered records from the Parquet file.
    
    Raises:
        HTTPException: If the file is not found or any other error occurs.
    """
    
    # Check the end of the name
    if not data.file_path.endswith("{postfix}"):
        raise HTTPException(status_code=403, detail=f"File does not end with {postfix}")
    
    # Determine if file is on S3 or local filesystem
    if s3_resource is not None:
        logging.info("Reading s3 file from %s %s", config["S3_BUCKET"], data.file_path)
        #bucket, key = data.file_path[5:].split("/", 1)
        bucket = s3_resource.Bucket(config["S3_BUCKET"])
        for b in s3_resource.buckets.all():
            print(b)
        logging.info("About to download")
        indata = BytesIO()
        bucket.download_fileobj(data.file_path, indata)
        df = pd.read_parquet(indata)
        logging.info("DF read from %s", data.file_path)
    elif os.path.exists(data.file_path):
        if data.file_path.startswith(f"{local_prefix}"):
            df = pd.read_parquet(data.file_path)
        else:
            raise HTTPException(status_code=403, detail=f"File does not start with {local_prefix}")
    else:
        raise HTTPException(status_code=404, detail=f"File {data.file_path} not found")
    
    # Apply query
    result_df = df.query(data.query)
    return result_df.to_json(date_format='iso')