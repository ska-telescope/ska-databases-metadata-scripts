# Query parquet webservice (qpq-ws) 
A webservice to query local or s3 parquet files

# Configuration
You will need a .env file with this s3 information:
```
S3_BUCKET=the bucket name
ENDPOINT_URL=the endpoint URL
ACCESS_KEY=the access key
SECRET_KEY=the secret key
```

# Caveats
* If using local file, the location of the file must start with data/ to avoid exposing sensitive information.
* The filename must end with .parquet

# Demo data
There is a 56K parquet file called att_array_devdouble_2025-01-13_2025-10-20.parquet on the data directory.

# Querying the service
Here is an example of a query:
```curl -X 'POST' \
  'http://localhost:8000/query' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "file_path": "data/att_array_devdouble_2025-01-13_2025-10-20.parquet",
  "query": "data_time<'\''2025-02-01T00:00:00+00'\''"
}'```