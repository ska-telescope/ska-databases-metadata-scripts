import sys
import base64

def processOne(username, password, secretname):
	secretname = secretname.replace("\n","")
	out = {"sql": "", "secret": "", "secret64": "", "secretname": secretname}
	p64 = bytes(password, "UTF-8")
	password64 = base64.b64encode(p64)
	out["sql"] = f"ALTER USER {username} PASSWORD '{password}';"
	out["secret"] = f"  {secretname}: {password}"
	password64 = password64.decode()
	out["secret64"] = f"  {secretname}: {password64}"
	return out

def display(myArray):
	for data in myArray:
		print(data["secretname"])
		print("##secret")
		print(data["secret"])
		print("##secret encoded")
		print(data["secret64"])
		print("--SQL")
		print(data["sql"])
		print()

def main():
	all = []
	with open("data.txt", "r") as aFile:
		for line in aFile.readlines():
			row = line.split(",")
			out = processOne(row[0], row[1], row[2])
			all.append(out)
	display(all)

if __name__ == "__main__":
	main()
