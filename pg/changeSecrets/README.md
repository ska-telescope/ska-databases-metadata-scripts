# A tool to change timescale passwords

This is a script that creates things needed to change timescaledb passwords. 

It will read information from data.txt and it will create the relevant secret entries encoded in base64 and not. 

The SQL needed to change the passwords to the new values is also shown.

## Usage
Run:
```
python3 util.py
PATRONI_SUPERUSER_PASSWORD
##secret
  PATRONI_SUPERUSER_PASSWORD: pass1
##secret encoded
  PATRONI_SUPERUSER_PASSWORD: cGFzczE=
--SQL
ALTER USER postgres PASSWORD 'pass1';

PATRONI_SUPERUSER_PASSWORD
##secret
  PATRONI_SUPERUSER_PASSWORD: pass2
##secret encoded
  PATRONI_SUPERUSER_PASSWORD: cGFzczI=
--SQL
ALTER USER admin PASSWORD 'pass2';

PATRONI_REPLICATION_PASSWORD
##secret
  PATRONI_REPLICATION_PASSWORD: pass3
##secret encoded
  PATRONI_REPLICATION_PASSWORD: cGFzczM=
--SQL
ALTER USER replication PASSWORD 'pass3';
```

Please change the content of data.txt