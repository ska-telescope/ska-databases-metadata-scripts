#!/bin/sh
src=a-db-pod-here
ns=some-namespace-here
outfile=bkp
dirout=bucketname:mybackups

##### No more config #####
date=`date +"%d%m%y_%H%M%S"`

kubectl exec ${src} -n ${ns} -- sh -c 'pg_dumpall' | zstd | rclone --config rc.cfg --log-file=${ns}-${src}-${outfile}_${date}.log -v rcat ${dirout}/${ns}-${src}-${outfile}_${date}.sql.zstd
