A script to dump all database and send them to an s3 bucket

It needs:
* rclone on the server running this.
* rclone bucket configuration on that server. That file is named rc.cfg. 
* pg_dumpall on the pod running this

Configure the first line of the script and run it.

