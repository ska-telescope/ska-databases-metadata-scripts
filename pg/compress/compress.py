'''I can check if there is compression and compress'''
import os
import psycopg2
import psycopg2.errors

def compress(table, connection, key="device_id"):
    '''I do the compression on a table'''
    sqls = []
    sqls.append(f"""ALTER TABLE {table} SET (timescaledb.compress,
            timescaledb.compress_segmentby = %s)""")
    sqls.append("""SELECT add_compression_policy(%s, INTERVAL '7 days')""")
    cursor = connection.cursor()
    print(sqls[0], key)
    print(sqls[1], table)
    cursor.execute(sqls[0], (key,))
    cursor.execute(sqls[1], (table,))
    connection.commit()

def get_compression(table, connection):
    '''I see if there is compression'''
    timescale = [True, None]
    sql = """SELECT * FROM timescaledb_information.jobs where proc_name=%s"""
    cursor = connection.cursor()
    print(sql, table)
    try:
        cursor.execute(sql, (table,))
        out = cursor.fetchone()
        print(out)
    except psycopg2.errors.lookup("UNDEFINED_TABLE"):
        timescale = [False, None]
    return timescale
        
def main():
    '''I'm the main'''
    user=os.getenv("USER", "value does not exist!")
    passwd=os.getenv("PASSWORD", "value does not exist!")
    host=os.getenv("HOST", "value does not exist!")
    db=os.getenv("DB", "value does not exist!")
    port = os.getenv("PORT", "value does not exist!")
    tablename = os.getenv("TABLE", "value does not exist!")
    key = os.getenv("KEY", "value does not exist!")
    if (user[0] is None or passwd[0] is None or host[0] is None or
        port[0] is None or db[0] is None):
        print("Missing variable. Please have USER, PASSWORD, HOST, DB, PORT and TABLE.")
        exit(1)
    print(f"{user}@{host}:{port}/{db} on table {tablename}")
    conn = psycopg2.connect(user=user,
                            password=passwd, host=host,
                            port=int(port), dbname=db)
    #compress(tablename, conn)
    ts = get_compression(tablename, conn)
    if ts[0]:
        #cond2
        compress(tablename, conn, key)

if __name__ == "__main__":
    main()
