# What
This is a script to compress an hyperscale table on timescale. Timescale is a time series extension for PostgreSQL.

# The script
The code is on compress.py. It will read variables specifies in the next topic. It depends on:
* python3
* psycopg2

You will need direct access to the timescale IP and port. The hypertable should already be there. Your user must have the privileges over that table. The default compression interval is 7 days. If something fails it, the script will crash. 

# Variables

You will need to setup these variables:
* USER: the user that will run the command
* PASSWORD: its password
* HOST: the host where timescale is
* PORT: the host's port
* DB: the database where the table is
* TABLE: the table name
* KEY: the partition key

Please check the testpy.sh script

# Container
To build it run:
`make build`
Please have a look at the run.sh script. It will invoke the runpy.sh script: You can also run:
`make run`
