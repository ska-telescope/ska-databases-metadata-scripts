'''I create the bucket'''
import boto3
import os
from dotenv import load_dotenv

def load_config():
    load_dotenv()
    return {
        "ENDPOINT_URL": os.getenv("ENDPOINT_URL"),
        "S3_BUCKET": os.getenv("S3_BUCKET"),
        "S3_KEY": os.getenv("S3_KEY", "output.parquet"),
        "ACCESS_KEY": os.getenv("ACCESS_KEY"),
        "SECRET_KEY": os.getenv("SECRET_KEY")
    }
    
def lsb(config):
    s3 = boto3.resource(
    's3',
    endpoint_url=config["ENDPOINT_URL"],  # Use your Ceph RGW endpoint
    aws_access_key_id=config["ACCESS_KEY"],
    aws_secret_access_key=config["SECRET_KEY"]
    )
    s3.create_bucket(Bucket=config["S3_BUCKET"])
    for bucket in s3.buckets.all():
        print(bucket.name)

def main():
    config = load_config()
    lsb(config)
    
if __name__ == "__main__":
    main()