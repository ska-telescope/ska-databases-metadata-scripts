# PostgreSQL to parquet file to an s3 bucket
A tool to export from a PostgreSQL table to parquet and eventually send it to an s3 bucket. The export will be from start to end date.

## Configuration
You must have a .env file with:
```
"DB_HOST": the db hostname,
"DB_PORT": db port, default is 5432,
"DB_NAME": db name,
"DB_USER": db username,
"DB_PASSWORD": db username's password,
"NAMESPACE": namespace,
"DATADIR": data directory, default is ./data,
"DATACENTRE": the datacentre name,
"TABLE_NAME": the table name,
"DATE_COLUMN": the time column name,
"START_DATE": a start date,
"END_DATE": an end date,
"CHUNK_SIZE": chunk size, default is 10.000 rows,
"ENDPOINT_URL": an s3 endpoint,
"S3_BUCKET": a bucket name,
"ACCESS_KEY": s3 access key,
"SECRET_KEY": s3 secret key
```

You can create the bucket with lsb.py if needed.

