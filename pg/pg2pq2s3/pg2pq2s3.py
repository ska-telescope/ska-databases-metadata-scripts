import sys
import os
import logging
import psycopg2
import pandas as pd
import pyarrow.parquet as pq
import boto3
from dotenv import load_dotenv

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def load_config():
    load_dotenv()
    return {
        "DB_HOST": os.getenv("DB_HOST"),
        "DB_PORT": os.getenv("DB_PORT", "5432"),
        "DB_NAME": os.getenv("DB_NAME"),
        "DB_USER": os.getenv("DB_USER"),
        "DB_PASSWORD": os.getenv("DB_PASSWORD"),
        "NAMESPACE": os.getenv("NAMESPACE"),
        "DATADIR": os.getenv("DATADIR", "./data"),
        "DATACENTRE": os.getenv("DATACENTRE"),
        "TABLE_NAME": os.getenv("TABLE_NAME"),
        "DATE_COLUMN": os.getenv("DATE_COLUMN", "created_at"),
        "START_DATE": os.getenv("START_DATE"),
        "END_DATE": os.getenv("END_DATE"),
        "CHUNK_SIZE": int(os.getenv("CHUNK_SIZE", "10000")),
        "ENDPOINT_URL": os.getenv("ENDPOINT_URL"),
        "S3_BUCKET": os.getenv("S3_BUCKET"),
        "S3_KEY": os.getenv("S3_KEY", "output.parquet"),
        "ACCESS_KEY": os.getenv("ACCESS_KEY"),
        "SECRET_KEY": os.getenv("SECRET_KEY")
    }

def print_instructions():
    print(f"""
    Instructions to run the script:
    1. Create a .env file with the following content:
       DB_HOST=your_postgres_host
       DB_PORT=5432
       DB_NAME=your_database
       DB_USER=your_user
       DB_PASSWORD=your_password
       DATACENTRE=datacentre
       NAMESPACE=namespace
       TABLE_NAME=your_table
       DATE_COLUMN=your_timestamp_column
       START_DATE=YYYY-MM-DD
       END_DATE=YYYY-MM-DD
       CHUNK_SIZE=10000
       ENDPOINT_URL=s3 endpoint
       S3_BUCKET=your_s3_bucket_name
       S3_KEY=output.parquet
       ACCESS_KEY=
       SECRET_KEY=
    1. Run the script using:
       python {sys.argv[0]}
    """)

def get_output_file(config):
    predir = os.path.join(config["DATADIR"], config["DATACENTRE"], config["NAMESPACE"], config["START_DATE"])
    os.makedirs(predir, exist_ok=True)
    return os.path.join(predir, f"{config['TABLE_NAME']}_{config['START_DATE']}_{config['END_DATE']}.parquet")

def export_to_parquet(config, output_file):
    conn = psycopg2.connect(
        host=config["DB_HOST"],
        port=config["DB_PORT"],
        dbname=config["DB_NAME"],
        user=config["DB_USER"],
        password=config["DB_PASSWORD"]
    )
    
    query = f"""
        SELECT * FROM {config['TABLE_NAME']} 
        WHERE {config['DATE_COLUMN']} BETWEEN %s AND %s
    """
    
    with conn.cursor() as cursor:
        cursor.execute(query, (config['START_DATE'], config['END_DATE']))
        
        while True:
            rows = cursor.fetchmany(config['CHUNK_SIZE'])
            if not rows:
                break
            df = pd.DataFrame(rows, columns=[desc[0] for desc in cursor.description])
            df.to_parquet(output_file, engine='pyarrow')
            logging.info(f"Processed {len(rows)} rows...")

    
    conn.close()
    logging.info(f"Data exported to {output_file}")

def upload_to_s3(output_file, config):
    if config["S3_BUCKET"]:
        s3_client = boto3.resource("s3",
            endpoint_url = config["ENDPOINT_URL"],
            aws_access_key_id = config["ACCESS_KEY"],
            aws_secret_access_key = config["SECRET_KEY"],
            aws_session_token=None,
            config=boto3.session.Config(signature_version='s3v4'),
            verify=False
            )
        logging.info("Src: %s, dest: %s", output_file, output_file.replace("./",""))
        bucket = s3_client.Bucket(config["S3_BUCKET"])
        bucket.upload_file(Filename=output_file, Key=output_file.replace("./",""))
        logging.info(f"File uploaded to s3://{config['S3_BUCKET']}/{output_file.replace("./","")}")

def main():
    config = load_config()
    if not config["TABLE_NAME"]:
        print("Error: TABLE_NAME is missing from configuration.")
        print_instructions()
        return
    logging.info("Starting data export process...")
    output_file = get_output_file(config)
    export_to_parquet(config, output_file)
    upload_to_s3(output_file, config)
    logging.info("Process completed successfully.")

if __name__ == "__main__":
    main()

