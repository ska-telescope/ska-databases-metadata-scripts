# Compare

This is a compare script to be run at source and destination. Given an connection string, for each table on the public schema, it will show the number of rows, the min time and the max time if a timestamp is found. Here is an example:
```
 TABLENAME|ROWS|MINTIME|MAXTIME
 'att_scalar_devstring|90133215|2023-09-06 15:57:02.912583+00:00|2024-08-20 02:48:39.166882+00:00'
```
You can diff two outputs to spot problems.

# Configuration
Create a cfg.py file with the definition of the connection string constr.
