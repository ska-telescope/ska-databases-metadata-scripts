import psycopg2
from pprint import pprint
from cfg import constr

conn = psycopg2.connect(constr)

def get_tables(schema="public"):
  cur = conn.cursor()
  SQL = "SELECT tablename FROM pg_catalog.pg_tables where schemaname=%s order by 1"
  cur.execute(SQL,(schema,))
  out = []
  for t in cur.fetchall():
    out.append(t[0])
  return out

def check_date(atable):
  SQL = """
SELECT column_name, data_type
FROM information_schema.columns
WHERE table_name = %s
  AND data_type IN ('timestamp without time zone', 'timestamp with time zone', 'date');
"""
  cur = conn.cursor()
  cur.execute(SQL,(atable,))
  datetime_columns = cur.fetchall()

  # Print the results
  if datetime_columns:
    for column in datetime_columns:
        return {"table": atable, "date": column[0]}
  cur.close()
  return {"table": atable, "date": None}

def check(data):
  SQL = f"""SELECT '{data["table"]}', COUNT(*), 'None', 'None' FROM {data["table"]}"""
  if data["date"] != None:
    SQL = f"""SELECT '{data["table"]}', COUNT(*), min({data["date"]}), max({data["date"]})  FROM {data["table"]}"""
  cur = conn.cursor()
  cur.execute(SQL)
  res = cur.fetchone()
  out = f"""{res[0]}|{res[1]}|{res[2]}|{res[3]}"""
  cur.close()
  return out

def main():
  tables = get_tables()
  dates = []
  for table in tables:
    dates.append(check_date(table))
  output = []
  for date in dates:
    output.append(check(date))
  pprint(output)
  conn.close()

if __name__ == "__main__":
  main()
