'''I can create SQL'''
import argparse
from helper import showme, mkdata

def mkcmd(data, psql=False, schema=False):
    '''I put all the commands together'''
    preextra = ""
    postextra = ""
    if psql:
        preextra = "psql postgres << EOSQL\n"
        postextra = "\nEOSQL\n"
    out =  f"{preextra}{data['create']}\n{data['grant']}\n{data['schema']}{postextra}"
    if not schema:
        out =  f"{preextra}{data['create']}\n{data['grant']}\n{postextra}"
    return out

def opts():
    '''The options'''
    parser = argparse.ArgumentParser(description="A tool to make PG  accounts")
    parser.add_argument("file", help="File with data to be processed")
    parser.add_argument("--schema", action="store_true", help="Write schema creation when present")
    parser.add_argument("--psql", action="store_true", help="Write psql code when present")
    parser.add_argument("--print", action="store_true", help="Print the user data")
    args = parser.parse_args()
    return args

def main():
    '''I am the main'''
    args = opts()
    datas = mkdata(args.file)
    for data in datas:
        print(mkcmd(data, args.psql, args.schema))
    if args.print:
        print(showme(datas))

if __name__ == "__main__":
    main()
