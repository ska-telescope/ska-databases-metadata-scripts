# A tool to create accounts

This is a code to create users on a postgresql cluster (PG). You need to connecto to the container running the PG and run this script output.

# Usage

```
usage: doit.py [-h] [--schema] [--psql] [--print] file

A tool to make PG accounts

positional arguments:
  file        File with data to be processed

options:
  -h, --help  show this help message and exit
  --schema    Write schema creation when present
  --psql      Write psql code when present
  --print     Print the user data
```

The script will create the username with a random password. It will have all the permissions on the database. The database has to be created before the user. Adding the ```--schema``` option will create a schema for that user. The schema name will match the username. The ```--psql``` option will allow you to run this as a script on the PG server.

The extras.py script allows to give permissions RW or RO to an existing account on other schemas in the same database.
ß
The file must have a list of accounts to be created. It must have an username and a database name, separated by commas with no spaces and one user per line. EG:
```
ACOUNT,DATABASE,EXTRASCHEMA1,EXTRASCHEMA2,...
myuser1,testdb1,extraschema1,extraschema2,...
```
Extra schemas are just handled by the extras scripts.


# Output of the tool

```
python doit.py --schema --psql ./testfile
--- Working on account myuser
mkgrant myuser, testdb
psql postgres << EOSQL
CREATE USER myuser WITH PASSWORD 'somerandompassword';
GRANT ALL PRIVILEGES ON DATABASE testdb TO myuser;
\c testdb
CREATE SCHEMA myuser AUTHORIZATION myuser;
EOSQL
```
