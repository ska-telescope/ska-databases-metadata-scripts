'''I help the main'''
import secrets
import string
from jinja2 import Environment, FileSystemLoader

environment = Environment(loader=FileSystemLoader("templates/"))

def mkdata(afile, schema=True):
    '''I create the data from a file'''
    accounts = readaccounts(afile)
    alldata = []
    for line in accounts:
        passw = mkpassword()
        line = line.replace('\n','').replace(" ","")
        data = line.split(",")
        account = data[0]
        database = data[1]
        extraschemas = []
        if (len(data)>2 ):
            for idx in range(2, len(data)):
                edata = data[idx]
                extraschemas.append(edata)
        schema = account
        #if len(data)>2:
        #    schema = data[2]
        print(f"--- Working on account {account}")
        newdata = {"username": account, "password": passw,
                   "create": create(account, passw),
                   "grant": mkgrant(account, database),
                   "database": database,
                   "schemaname": schema,
                   "extraschemas": extraschemas,
                   "schema": mkschema(account, schema, database=database) }
        alldata.append(newdata)
    return alldata

def mkpassword(size=20):
    '''Creates passwords'''
    alphabet = string.ascii_letters + string.digits
    password = ''.join(secrets.choice(alphabet) for i in range(size))
    return password

def readaccounts(afile):
    '''Read the accounts file'''
    accounts = []
    with open(afile, encoding='utf-8') as f:
        lines = f.readlines()
        for line in lines:
            accounts.append(line)
    return accounts

def grantaccountschema(account, schemaname, end=";"):
    '''Create an account'''
    template = environment.get_template("perm.sql.j2")
    content = template.render({"username": account, 
                               "schemaname": schemaname,
                               "end": end})
    return content

def grantroaccountschema(account, schemaname, end=";"):
    '''Create an account'''
    template = environment.get_template("roperm.sql.j2")
    content = template.render({"username": account, 
                               "schemaname": schemaname,
                               "end": end})
    return content

def grantrwaccountschema(account, schemaname, defaults=True, end=";"):
    '''Create an account'''
    template = environment.get_template("rwperm.sql.j2")
    content = template.render({"username": account, 
                               "schemaname": schemaname,
                               "changedefaults": defaults,
                               "end": end})
    return content

def mkextra(account, schemaname, rw, defaults=False, end=";"):
    '''I do the extra stuff'''
    out = grantaccountschema(account, schemaname)
    perm = f"{out}"
    if rw:
        extra = grantrwaccountschema(account, schemaname, defaults)
    else:
        extra = grantroaccountschema(account, schemaname)
    return f"{perm}\n{extra}"

def create(account, password, end=";"):
    '''Create an account'''
    template = environment.get_template("mk.sql.j2")
    content = template.render({"username": account, 
                               "password": password,
                               "end": end})
    return content

def mkgrant(username, database=None, end=";"):
    '''I create grants'''
    if database is None:
        database = username
    template = environment.get_template("grt.sql.j2")
    content = template.render({"username": username, 
                               "database": database,
                               "end": end})
    return content

def mkschema(username, schema, database="", end=";"):
    '''I create schemas'''
    template = environment.get_template("schema.sql.j2")
    if database!="":
        database = f"\\c {database}\n"
        content = template.render({"username":username, 
                               "schema": schema,
                               "database": database,
                               "end": end})
    return content

def showme(datas):
    '''Print data'''
    outstr = ""
    for data in datas:
        outstr = f"{outstr}\n---{data['username']},{data['password']},{data['database']},{data['schemaname']}"
    return outstr
