'''I can create SQL to look at other schemas'''
import argparse
from helper import showme, mkdata, mkextra

def mkcmdextra(data, psql=False, rw=True, default=True, schema=True):
    '''I put all the commands together'''
    preextra = ""
    postextra = ""
    print(f"def: {default}")
    allstr = ""
    for d in data:
        for schema in d["extraschemas"]:
            cmd = mkextra(d['username'], schema, rw, default)
            allstr = f"{allstr}\n---\n{cmd}"
    if psql:
        preextra = "psql postgres << EOSQL\n"
        postextra = "\nEOSQL\n"
    out =  f"{preextra}{allstr}{postextra}"
    if not schema:
        out =  f"{preextra}{allstr}\n{postextra}"
    print(out)
    return out

def opts():
    '''The options'''
    parser = argparse.ArgumentParser(description="A tool to make add access to an existing PG account on schemas")
    parser.add_argument("file", help="File with data to be processed")
    parser.add_argument("--rw", action="store_true", help="Add rw permissions on extras schemas")
    parser.add_argument("--default", action="store_true", help="Changes schema defaults when present")
    parser.add_argument("--psql", action="store_true", help="Write psql code when present")
    parser.add_argument("--print", action="store_true", help="Print the user data")
    args = parser.parse_args()
    return args

def main():
    '''I am the main'''
    args = opts()
    datas = mkdata(args.file)
    mkcmdextra(datas, args.psql, args.rw, args.default)
    if args.print:
        print(showme(datas))

if __name__ == "__main__":
    main()
