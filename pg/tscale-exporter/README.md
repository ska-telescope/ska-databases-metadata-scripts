# Tscale exporter
This is an exporter for timescale. It will present its metrics on port 8000 on /metrics.

# Configuration
There is a config yaml file. Change the relevant parameters there.

# Queries executed
There is a file with all the queries on ```queries/all_queries.sql```.

The first line of a query must contain the name of the metric and the second one a description. 
The third line is where the query starts. This is an example

  ```
  --- metric_name
  --- Metric description
  SELECT COUNT(*) FROM my_table;
  ```