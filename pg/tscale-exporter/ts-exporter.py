"""
Prometheus PostgreSQL Exporter

This script reads custom SQL queries from a single file, executes them asynchronously,
and exposes the results as Prometheus metrics.

Features:
- Reads database configuration from a YAML file.
- Parses queries from a single file, with each query block separated by an empty line.
- Extracts the metric name and description from the first two lines of each query block.
- Uses asyncpg for asynchronous database queries.
- Exposes metrics via an HTTP server on port 8000 for Prometheus to scrape.

Configuration:
- `config.yaml` contains database credentials, pool size, scrape interval, and query file location.
- The query file should have multiple query blocks, each structured as:
  ```
  ---metric_name
  ---Metric description
  SELECT NAME, COUNT(*) FROM my_table;
- That query will return many labels with distinct values
  ```
"""

import asyncio
import asyncpg
import yaml
from prometheus_client import Gauge, start_http_server

# Load configuration from YAML
def load_config(config_path="config.yaml"):
    """Load database and query configuration from a YAML file."""
    with open(config_path, "r") as file:
        return yaml.safe_load(file)

# Parse queries from a single file
def parse_queries(query_file):
    """Read queries from a single file, extracting metric names, descriptions, and SQL queries."""
    metrics = {}
    with open(query_file, "r") as file:
        content = file.read().strip()
        query_blocks = content.split("\n\n")
        
        for block in query_blocks:
            lines = block.strip().split("\n")
            if len(lines) < 3:
                continue
            
            name = lines[0].strip().lstrip("--- ")
            description = lines[1].strip().lstrip("--- ")
            query = "\n".join(lines[2:])
            metrics[name] = {"description": description, "query": query}
    
    return metrics

# Initialize Gauges dynamically with labels
def initialize_metrics(metrics):
  """Create Prometheus Gauge metrics dynamically based on parsed queries."""
  gauges = {}
  for name, data in metrics.items():
      gauges[name] = Gauge(name, data["description"], ["label"])  # Generic label field
  return gauges

# Fetch query results asynchronously
async def fetch_metrics(pool, metrics, gauges):
  """Execute queries asynchronously and update Prometheus Gauges."""
  async with pool.acquire() as conn:
      for name, data in metrics.items():
          try:
              results = await conn.fetch(data["query"])  # Fetch all rows
              
              # Clear previous values to prevent stale metrics
              gauges[name]._metrics.clear()
              
              for result in results:
                  label_value = result[0]  # First column as label (table name)
                  count_value = result[1]  # Second column as count
                  gauges[name].labels(label=label_value).set(count_value)
                  
          except Exception as e:
              print(f"Error fetching data for {name}: {e}")

# Main async loop
async def main():
    """Main function to load config, initialize metrics, and start the exporter."""
    config = load_config()
    metrics = parse_queries(config["query_file"])
    gauges = initialize_metrics(metrics)
    
    pool = await asyncpg.create_pool(
        user=config["db"]["user"],
        password=config["db"]["password"],
        database=config["db"]["database"],
        host=config["db"]["host"],
        port=config["db"].get("port", 5432),
        min_size=config["db"].get("pool_min_size", 1),
        max_size=config["db"].get("pool_max_size", 10),
        command_timeout=10
    )
    
    start_http_server(8000)  # Expose metrics on port 8000
    print("Prometheus exporter running on port 8000")
    
    while True:
        await fetch_metrics(pool, metrics, gauges)
        await asyncio.sleep(config.get("scrape_interval", 15))

if __name__ == "__main__":
    asyncio.run(main())
