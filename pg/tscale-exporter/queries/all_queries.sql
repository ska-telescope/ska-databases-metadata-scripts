--- att_array_devboolean
--- att_array_devboolean rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devboolean",
  COUNT(*) AS rate
FROM att_array_devboolean
JOIN att_conf ON att_array_devboolean.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devdouble
--- att_array_devdouble rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devdouble",
  COUNT(*) AS rate
FROM att_array_devdouble
JOIN att_conf ON att_array_devdouble.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devencoded
--- att_array_devencoded rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devencoded",
  COUNT(*) AS rate
FROM att_array_devencoded
JOIN att_conf ON att_array_devencoded.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devenum
--- att_array_devenum rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devenum",
  COUNT(*) AS rate
FROM att_array_devenum
JOIN att_conf ON att_array_devenum.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devfloat
--- att_array_devfloat rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devfloat",
  COUNT(*) AS rate
FROM att_array_devfloat
JOIN att_conf ON att_array_devfloat.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devlong
--- att_array_devlong rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devlong",
  COUNT(*) AS rate
FROM att_array_devlong
JOIN att_conf ON att_array_devlong.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devlong64
--- att_array_devlong64 rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devlong64",
  COUNT(*) AS rate
FROM att_array_devlong64
JOIN att_conf ON att_array_devlong64.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devshort
--- att_array_devshort rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devshort",
  COUNT(*) AS rate
FROM att_array_devshort
JOIN att_conf ON att_array_devshort.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devstate
--- att_array_devstate rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devstate",
  COUNT(*) AS rate
FROM att_array_devstate
JOIN att_conf ON att_array_devstate.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devstring
--- att_array_devstring rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devstring",
  COUNT(*) AS rate
FROM att_array_devstring
JOIN att_conf ON att_array_devstring.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devuchar
--- att_array_devuchar rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devuchar",
  COUNT(*) AS rate
FROM att_array_devuchar
JOIN att_conf ON att_array_devuchar.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devulong
--- att_array_devulong rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devulong",
  COUNT(*) AS rate
FROM att_array_devulong
JOIN att_conf ON att_array_devulong.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devulong64
--- att_array_devulong64 rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devulong64",
  COUNT(*) AS rate
FROM att_array_devulong64
JOIN att_conf ON att_array_devulong64.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_array_devushort
--- att_array_devushort rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_array_devushort",
  COUNT(*) AS rate
FROM att_array_devushort
JOIN att_conf ON att_array_devushort.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devboolean
--- att_image_devboolean rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devboolean",
  COUNT(*) AS rate
FROM att_image_devboolean
JOIN att_conf ON att_image_devboolean.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devdouble
--- att_image_devdouble rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devdouble",
  COUNT(*) AS rate
FROM att_image_devdouble
JOIN att_conf ON att_image_devdouble.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devencoded
--- att_image_devencoded rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devencoded",
  COUNT(*) AS rate
FROM att_image_devencoded
JOIN att_conf ON att_image_devencoded.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devenum
--- att_image_devenum rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devenum",
  COUNT(*) AS rate
FROM att_image_devenum
JOIN att_conf ON att_image_devenum.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devfloat
--- att_image_devfloat rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devfloat",
  COUNT(*) AS rate
FROM att_image_devfloat
JOIN att_conf ON att_image_devfloat.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devlong
--- att_image_devlong rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devlong",
  COUNT(*) AS rate
FROM att_image_devlong
JOIN att_conf ON att_image_devlong.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devlong64
--- att_image_devlong64 rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devlong64",
  COUNT(*) AS rate
FROM att_image_devlong64
JOIN att_conf ON att_image_devlong64.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devshort
--- att_image_devshort rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devshort",
  COUNT(*) AS rate
FROM att_image_devshort
JOIN att_conf ON att_image_devshort.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devstate
--- att_image_devstate rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devstate",
  COUNT(*) AS rate
FROM att_image_devstate
JOIN att_conf ON att_image_devstate.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devstring
--- att_image_devstring rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devstring",
  COUNT(*) AS rate
FROM att_image_devstring
JOIN att_conf ON att_image_devstring.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devuchar
--- att_image_devuchar rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devuchar",
  COUNT(*) AS rate
FROM att_image_devuchar
JOIN att_conf ON att_image_devuchar.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devulong
--- att_image_devulong rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devulong",
  COUNT(*) AS rate
FROM att_image_devulong
JOIN att_conf ON att_image_devulong.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devulong64
--- att_image_devulong64 rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devulong64",
  COUNT(*) AS rate
FROM att_image_devulong64
JOIN att_conf ON att_image_devulong64.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_image_devushort
--- att_image_devushort rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_image_devushort",
  COUNT(*) AS rate
FROM att_image_devushort
JOIN att_conf ON att_image_devushort.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devboolean
--- att_scalar_devboolean rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devboolean",
  COUNT(*) AS rate
FROM att_scalar_devboolean
JOIN att_conf ON att_scalar_devboolean.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devdouble
--- att_scalar_devdouble rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devdouble",
  COUNT(*) AS rate
FROM att_scalar_devdouble
JOIN att_conf ON att_scalar_devdouble.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devencoded
--- att_scalar_devencoded rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devencoded",
  COUNT(*) AS rate
FROM att_scalar_devencoded
JOIN att_conf ON att_scalar_devencoded.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devenum
--- att_scalar_devenum rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devenum",
  COUNT(*) AS rate
FROM att_scalar_devenum
JOIN att_conf ON att_scalar_devenum.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devfloat
--- att_scalar_devfloat rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devfloat",
  COUNT(*) AS rate
FROM att_scalar_devfloat
JOIN att_conf ON att_scalar_devfloat.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devlong
--- att_scalar_devlong rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devlong",
  COUNT(*) AS rate
FROM att_scalar_devlong
JOIN att_conf ON att_scalar_devlong.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devlong64
--- att_scalar_devlong64 rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devlong64",
  COUNT(*) AS rate
FROM att_scalar_devlong64
JOIN att_conf ON att_scalar_devlong64.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devshort
--- att_scalar_devshort rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devshort",
  COUNT(*) AS rate
FROM att_scalar_devshort
JOIN att_conf ON att_scalar_devshort.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devstate
--- att_scalar_devstate rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devstate",
  COUNT(*) AS rate
FROM att_scalar_devstate
JOIN att_conf ON att_scalar_devstate.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devstring
--- att_scalar_devstring rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devstring",
  COUNT(*) AS rate
FROM att_scalar_devstring
JOIN att_conf ON att_scalar_devstring.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devuchar
--- att_scalar_devuchar rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devuchar",
  COUNT(*) AS rate
FROM att_scalar_devuchar
JOIN att_conf ON att_scalar_devuchar.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devulong
--- att_scalar_devulong rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devulong",
  COUNT(*) AS rate
FROM att_scalar_devulong
JOIN att_conf ON att_scalar_devulong.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devulong64
--- att_scalar_devulong64 rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devulong64",
  COUNT(*) AS rate
FROM att_scalar_devulong64
JOIN att_conf ON att_scalar_devulong64.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;

--- att_scalar_devushort
--- att_scalar_devushort rate
SELECT CONCAT(domain, '/', family, '/', member) AS "att_scalar_devushort",
  COUNT(*) AS rate
FROM att_scalar_devushort
JOIN att_conf ON att_scalar_devushort.att_conf_id = att_conf.att_conf_id
WHERE data_time > now() - interval '1 day'
GROUP BY CONCAT(domain, '/', family, '/', member)
ORDER BY rate DESC
LIMIT 20;
