# This is a stackgres demo

## What is stackgres?
It is an opinionated kubernetes operator for PostgreSQL and patroni.

## Pre requisites
You will need:
* A laptop with 12GB of ram and disk to keep the images
* [minikube up and running](https://minikube.sigs.k8s.io/docs/start)
* helm, kubectl and yq installed
* An internet connection
And it also runs on MacOS!

## Steps
Go to
1. [install](install/): this will install the operator
2. [test cluster](testcluster/): this will install a 3 node postgresql
3. [connect an app](connectanapp): this will create a database, user and app inserting data
4. [using patroni](usingpatroni): patroni operations
5. [issue](issue): backups and try to generate a problem

## Answers
Please create a child page with your team and name at the end of [this page](https://confluence.skatelescope.org/display/SE/StackGres+training). We all learn from your answers.

# Feedback
Please send it to mauricio.zambrano@skao.int.
