# Installing a three nodes PostgreSQL instance

You will need to:
* Run ```minikube tunnel``` to be able to connect to LB services.
* Install a PostgreSQL client 16 on your server.

The ```mycluster.yaml``` file will install a 3 nodes PostgreSQL 16.8 patroni managed installation.
1. Check the stackgres architecture at https://stackgres.io/doc/1.5/intro/architecture/
2. Review the yaml definition of 3 node cluster defined on [mycluster.yaml](./mycluster.yaml) and install it by creating it.
3. Check what's happening with: ```kubectl get events -n stackgres-demo -w```
4. Check that the three pods were deployed: ```kubectl get pods -n stackgres-demo```
5. Check the documentation of the SGCluster object at https://stackgres.io/doc/1.5/reference/crd/sgcluster/

You should see:
```
NAME         READY   STATUS    RESTARTS   AGE
mytestpg-0   3/3     Running   0          4m21s
mytestpg-1   3/3     Running   0          2m28s
mytestpg-2   3/3     Running   0          2m10s
```

Read the SGCluster definition with ```kubectl get sgcluster mytestpg -n stackgres-demo -o yaml``` and list the availble services. Edit the sgcluster mytestpg to create a loadBalancer for the primary. Be sure to run ```minikube tunnel```. List the secrets with ```kubectl get secret mytestpg -n stackgres-demo -o yaml``` and get the administrator login and password. Connect as the administrator to your cluster with:
```psql -U postgres -h YOUR.TUNNEL.IP.HERE -p SVCPORT```
Run the following commands:
* ```\l```: list the databases
* ```\d```: list the tables on the current database
* ```\dx\```: list the PG extensions
* See the help ```\?```
* Do a query on one of the tables of the database

Optional: 
* enable monitoring by editing *two lines* on the SGCluster specs, see [here](https://stackgres.io/doc/1.5/administration/monitoring/). The default grafana credential is admin/prom-operator.
* Is the cluster being monitored? What is missing?
* After reading the patroni lab, plan for a minimal disruption restart with patronictl.

# Questions
* What does envoy do? How could I replace it?
* What is a pgBouncer? What would you use it for? How is the connection made (review the architecture diagram)?  
* What is patroni? Who is keeping the status of the PostgreSQL (leader/replica)?
* What objects were created by the SGCluster?
* What are the nonProductionOptions?
* Who's the primary?
* What are the names of the created PVCs?
* How would you expose the replicas? 
* What are the use cases of a replica node?
* How many containers are there in a single pod in your current deployment? Compare it with a production installation.
A full installation looks like this:
```
Defaulted container "patroni" out of: patroni, envoy, pgbouncer, prometheus-postgres-exporter, postgres-util, cluster-controller, setup-arbitrary-user (init), setup-scripts (init), relocate-binaries (init), pgbouncer-auth-file (init), cluster-reconciliation-cycle (init)
```
* What happens if you kill a primary pod?
* What happens if you kill a replica pod?
* What other options to connect to the cluster do you have?
* How do you increase the max number of connections?
* How do you define an SGCluster downtime?

# Takeaway

* The StackGres Architecture
* The SGCluster and associated objects
* Creating a multinode patroni deployment
* Connecting to the database
* Browsing database objects:
  * Databases
  * Tables