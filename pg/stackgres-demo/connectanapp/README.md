# Connect an app
We will create:
* A database and a schema
* Three users for that schema
* Start the app that inserts data
We will use the kubernetes way of doing things. All the steps could also be created with psql.

# Create a database and a schema
Schemas are like namespaces for database objects.
PG docs on
* Databases [here](https://www.postgresql.org/docs/current/sql-createdatabase.html). Databases have LOCALE information.
* Schemas [here](https://www.postgresql.org/docs/current/ddl-schemas.html)

## Status on start
Log on the database and list databases and roles:
* ```\l```
* ```\du```

## Creation kubernetes object
Sensitive script should be stored as secrets!
The database name is testdb and the schemaname is testapp. 
The roles will be:
* testadmin: can do anything on the schema testapp
* testro: can read all the tables on the schema testapp
* testwo: can insert on all the tables of the schema testapp
* testrw: cand read/write all the tables on the schema testapp 
testuser and its password changeme.

The SQL script for this would be:
```
---THE DATABASE
DROP DATABASE IF EXISTS testdb;
CREATE DATABASE testdb;
\c testdb
---NOW THE SCHEMA
CREATE SCHEMA testapp;
---NOW THE ROLES
CREATE ROLE testadmin;
CREATE ROLE testro;
CREATE ROLE testwo;
CREATE ROLE testrw;
GRANT ALL PRIVILEGES ON DATABASE testdb to postgres;
GRANT CONNECT ON DATABASE testdb to testadmin;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA testapp to testadmin;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA testapp to testadmin;
GRANT CONNECT ON DATABASE testdb to testro;
GRANT USAGE ON SCHEMA testapp to testro;
GRANT SELECT ON ALL TABLES IN SCHEMA testapp to testro;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA testapp to testro;
ALTER DEFAULT PRIVILEGES IN SCHEMA testapp GRANT SELECT ON TABLES to testro;
GRANT CONNECT ON DATABASE testdb to testwo;
GRANT USAGE ON SCHEMA testapp to testwo;
GRANT INSERT ON ALL TABLES IN SCHEMA testapp TO testwo;
ALTER DEFAULT PRIVILEGES IN SCHEMA testapp GRANT INSERT ON TABLES TO testwo;
---NOW THE USERS
CREATE USER userapp1 IN ROLE testadmin PASSWORD 'changeme1';
CREATE USER userroapp1 IN ROLE testro PASSWORD 'changeme2';
CREATE USER userwoapp1 IN ROLE testwo PASSWORD 'changeme3';
ALTER SCHEMA testapp OWNER TO testadmin;
```

We will use SGSCript to run this in two SGScripts:
```
kubectl apply -f - << EOYML
apiVersion: stackgres.io/v1
kind: SGScript
metadata:
  name: createdb-testdb
  namespace: stackgres-demo
spec:
 scripts:
 - name: createdb-testdb
   script: |
        drop database if exists testdb;
        create database testdb;
EOYML
```

```
kubectl apply -f - << EOYML
apiVersion: stackgres.io/v1
kind: SGScript
metadata:
  name: create-roles-testdb
  namespace: stackgres-demo
spec:
 scripts:
 - name: create-roles-testdb
   database: testdb
   script: |
    CREATE SCHEMA testapp;
    CREATE ROLE testadmin;
    CREATE ROLE testro;
    CREATE ROLE testwo;
    CREATE ROLE testrw;
    GRANT ALL PRIVILEGES ON DATABASE testdb to postgres;
    GRANT CONNECT ON DATABASE testdb to testadmin;
    GRANT USAGE, CREATE ON SCHEMA testapp to testadmin;
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA testapp to testadmin;
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA testapp to testadmin;
    GRANT CONNECT ON DATABASE testdb to testro;
    GRANT USAGE ON SCHEMA testapp to testro;
    GRANT SELECT ON ALL TABLES IN SCHEMA testapp to testro;
    GRANT SELECT ON ALL SEQUENCES IN SCHEMA testapp to testro;
    ALTER DEFAULT PRIVILEGES IN SCHEMA testapp GRANT SELECT ON TABLES to testro;
    GRANT CONNECT ON DATABASE testdb to testwo;
    GRANT USAGE ON SCHEMA testapp to testwo;
    GRANT INSERT ON ALL TABLES IN SCHEMA testapp TO testwo;
    ALTER DEFAULT PRIVILEGES IN SCHEMA testapp GRANT INSERT ON TABLES TO testwo;
EOYML
```

Edit the SGCluster to apply the two scripts you just created. See the script section.

Check again the databases and roles.

Create the user with psql:
* ```CREATE USER appadmin WITH PASSWORD 'changeme1' IN ROLE testadmin;```
* ```CREATE USER appro WITH PASSWORD 'changeme2' IN ROLE testro;```
* ```CREATE USER appwo WITH PASSWORD 'changeme3' IN ROLE testwo;```

It is sometimes better to match the role names with the username because the search_path includes by default ```'$users'```.

## Activities

* Test the users login.
* Who is the testapp owner?
* Create the delme table with this:
```
CREATE TABLE delme (
    id SERIAL,
    comment VARCHAR(200),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
```
* How do you fix this? 
* The default schema to be used is defined by search_path, run ```SHOW search_path;```
* For all the user set the default ```search_path``` to ```testapp,"$user",public```
* Try: ```\dt testapp.*```
* Insert some data with ```insert into delme(comment, created_at) values('hello skao', NOW());```
* Test what you can do with the different users
* Update some data with the wo role user. Something is missing? Update the relevant role. Tip: you need to do two things on the sequence and one on the table.
* Create a read/write role and create a new user with that role
* Delete the delme table

## Connect an app
The app is [here](https://gitlab.com/ska-telescope/ska-databases-metadata-scripts/-/tree/main/mariadb/inserter?ref_type=heads). Create the table with the admin user by using this [script](https://gitlab.com/ska-telescope/ska-databases-metadata-scripts/-/blob/main/mariadb/inserter/schema.postgres.sql?ref_type=heads). Use the insert only user to run this app. If you have problems, use the admin. Leave it running for the next Lab.
The ```.env``` file should look like this:
```
DB_TYPE=postgres
DB_HOST=ip.of.the.pg.leader
DB_PORT=port.of.the.pg.leader
DB_NAME=databasename
DB_SCHEMA=schemaname
```
You can create a virtualenv and run it from your laptop

## Row level policy
You can define row-level access policies for tables. This is out of the scope of this tutorial. Info [here](https://www.postgresql.org/docs/current/ddl-rowsecurity.html)

# Takeaway

* Creating a database and schema, understanding their use
* Roles and grants
* User creation and assign them to roles
* Connecting an app