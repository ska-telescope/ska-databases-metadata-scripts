# Installing the StackGres Operator

The StackGress operator is developed at [this gitlab repo](https://gitlab.com/ongresinc/stackgres).

Run the [```installme.sh```](./installme.sh) script. The operator will be deployed on the ```stackgres``` namespace.

List the pods on the stackgres-operator namespace. 
* How many are they? 
* What does each one of them do?
* Run the checks suggested by the installation [output-stackgres.txt](./output-stackgres.txt). Access the stackgres GUI.

Optional: install the prometheus stack to have automatic prometheus/grafana/alertmanager for stackgres. Run the [```monitoring.sh```](./monitoring.sh) script. See more details [here](https://stackgres.io/doc/1.5/administration/monitoring/). Installation output [here](./output-monitoring.txt). Cert manager will also be installed

Check the version of the operator installed with ```helm list -n stackgres```

# Uninstalling
Look at the documentation page on how to [uninstall](https://stackgres.io/doc/latest/administration/uninstall/).

# Operator upgrade
This is explained [here](https://stackgres.io/doc/1.5/administration/helm/upgrade/)

# Dependency
Certmanager is also installed to create certificates.

# Takeaway

* Installing and uninstalling the Stackgres Operator
