# How to handle database issues

The first and most important rule:

## Do a backup

You could use an [SGBackup object](https://stackgres.io/doc/1.5/administration/backups/) to do the backup to an s3 bucket. This would require to:
* Have a minio or s3 bucket access
* Configure the sgCluster with the s3 parameters
Once you have a backup you can [create a new instance from a backup](https://stackgres.io/doc/1.5/runbooks/restore-backup/).

On the other hand you can also use the pod tools to do a full or single database backup. 

On the SGCluster, enable the PostgresUtil pod.

Run a single database backup with:
```
kubectl exec mytestpg-0 -n stackgres-demo -c postgres-util -- pg_dump testdb | gzip > testdb.sql.gzip
```

Run the full backup:
```
kubectl exec mytestpg-0 -n stackgres-demo -c postgres-util -it -- pg_dumpall | zstd > full.sql.zst
```

# Create an issue
In this case we will try to generate a situation that requires a reinitialization of the nodes.

*WIP*

# Questions
* How do you configure an external s3 bucket?
* How do you do a periodic backup with the operator?
* How do you recover on a new cluster from an operator backup?
* What's the difference between pg_dumpall and pg_dump?

