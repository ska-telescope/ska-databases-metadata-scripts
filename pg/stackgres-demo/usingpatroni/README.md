# Using patroni

[Patroni](https://patroni.readthedocs.io/en/latest/) is a way to run HA in PostgreSQL. 

Operations you might face in production:
* Switchover
* Automatic failover
* Replica re-initialization

Stackgres does not handle patroni. In order to use it, you have to execute the commands on a stackgres node.

Please have the application running inserting 1 row per second.

## Switchover
This is a planned change of the primary and it is triggered by the administrator.
Follow [this tutorial](https://stackgres.io/doc/latest/administration/patroni/switchover/)

## Automated failover
This is an unplanned crash of a pod. Kubernetes will relaunch it. The pod might change of role.
Follow [this tutorial](https://stackgres.io/doc/latest/administration/patroni/failover/)

## Reinitialization
This is required when neither PostgreSQL or Patroni can catch-up with the primary.
Follow [this demo](https://stackgres.io/doc/latest/administration/patroni/reinit/)

# Questions
* What are the differences between a failover and a switchover?
* Who (software) can promote a node?
* How much time does it take to do a switchover?
* When is a reinitialization needed? Think of a time it happened in production. See our PostgreSQL Knowledge Base [here](https://confluence.skatelescope.org/display/SE/PostgreSQL+and+extensions+known+issues).
* Can you send patroni commands with the operator?
* How can you send commands to patroni?

# Takeaway
Patroni operations:
* Switchover
* Failover
* Reinitialization - when to use it.
