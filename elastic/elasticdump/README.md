# Elastic-dump
A container to export and import elastic indexes.

# Build the container
On the Makefile edit the ARCH and do a make build.

# Usage
You will need to have a key to connect to/from an elastic search.

```
docker run -it localhost/edump:latest elasticdump --input=https://some.kibana.in.our.net:9200/some-index-v1 --headers='{"Authorization": "ApiKey THELONGKEYSHOULDGOHERE"}' --output=$ | gz > some-index-v1.gz
```

# Repo location
https://github.com/elasticsearch-dump/elasticsearch-dump
