# README for the replication tester

## What

This is a python set of scripts to test database replication. At the moment, just PostgreSQL has been implemented. A new database can easily added by modifying [helper.py](./helper.py). 
You will need to have a database, an user, a schema for that user if using PostgreSQL.

## Configuration file

The configuration file is a typical configuration file with sections. There is an example on [dbs.conf](./dbs.conf). Here is a description of the sections.

### DEFAULT section
This provides the database type.

### use section
The dbs section values will be used to run script on all the database described there. Script and check can be executed if mt.py is used.

### init section
The scripts to create the neede objects (tables, sequences, etc...).

## Running the scripts

You should have the user created everywhere with the right permissions before using the scripts.

### Initialize the database
Run this:
```
python3 doit.py ./dbs.conf
```
This will run:
1. The script on the db database from the init section.
2. The script on the dbs databases defined in the use section.
### Create a conflict on the databases

Run this:
```
python3 mt.py ./dbs.conf script
```

### Use the check script

Run this:
```
python3 mt.py ./dbs.conf check
```

### Use the object generator
In this case there will be no SQL file will be used, it will use a generator to create meaningful data. This is currently implemented for the SBD object and its tab_oda_sbd table. It expects to run on the replication database, schema reptest1, user reptest1. 
First download the oda table definition with:
```
make download
```
Run the creation script:
```
python3 doit.py ./oda.conf
```
Add sbds:
```
python3 mt-generated.py oda.conf noscript
```