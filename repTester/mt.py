'''I execute commands in parallel from scripts'''
import threading
import logging
import sys
from helper import get_conf, run

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

class DoInDB(threading.Thread):
    '''A Class for doing MT stuff on a db'''
    def __init__(self, conf_file, db, cmd, name):
        threading.Thread.__init__(self)
        self.conf_file = conf_file
        self.db = db
        self.cmd = cmd
        self.name = name
        logging.debug("Thread %s, init conf file: %s, db: %s, cmd: %s", name, conf_file, db, cmd)

    def run(self):
        logging.debug("Starting thread %s, conf file: %s, db: %s, cmd: %s", self.name, 
                      self.conf_file, self.db, 
                      self.cmd)
        run(self.cmd, self.conf_file, self.db)
        logging.debug("end of thread %s", self.name)

def main():
    '''Another main'''
    config = get_conf(sys.argv[1])  
    dbs = config["use"]["dbs"].split(",") 
    script = config["use"][sys.argv[2]]     
    #todo_init = {}
    thrds = []
    n = 0
    for i in dbs:
        thrds.append(DoInDB(sys.argv[1], i, script, n))
        n = n+1     
    for i in thrds:
        i.start()
    for i in thrds:
        i.join()

if __name__ == "__main__":
    main()
