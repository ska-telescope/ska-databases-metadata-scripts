DROP TABLE IF EXISTS REPTEST ;

CREATE TABLE REPTEST(
id integer primary key,
data varchar(50),
created_on TIMESTAMP default CURRENT_TIMESTAMP,
location varchar(50)
);

--INSERT INTO REPTEST(id, data, location) VALUES(1, 'First data', 'site1');
--INSERT INTO REPTEST(id, data, location) VALUES(4, 'Second data', 'site1');
--INSERT INTO REPTEST(id, data, location) VALUES(7, 'Second data', 'site1');
