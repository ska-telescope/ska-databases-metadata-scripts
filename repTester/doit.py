"""A script to run this"""
import sys
import logging
from helper import get_conf, run

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

def main():
    """I do stuffs"""
    config = get_conf(sys.argv[1])
    init = {}
    init[config["init"]["db"]] = config["init"]["script"]
    run(init[config["init"]["db"]], sys.argv[1], config["init"]["db"])
    #read dbs to be used
    dbs = config["use"]["dbs"].split(",")
    #create queue for each database    
    todo_init = {}
    for i in dbs:
        todo_init[i] = config[i]["load"]
        run(config[i]["load"], sys.argv[1], i)
    print(todo_init)

if __name__ == "__main__":
    main()