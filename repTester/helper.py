"""A helper with functions"""
import sys
import logging
from configparser import ConfigParser
import datetime
import psycopg2 as db
from sbd import generate_test_sbd

cfg = ConfigParser()
config = {}

libs={"postgresql": {"lib":"psycopg2"}}

def get_conf(a_file):
    """Reads the configuration file values"""
    cfg.read(a_file)
    in_db = cfg["DEFAULT"]["database"]
    logging.info("%s", in_db)
    load_db = "import %s as db" % (libs[in_db]["lib"])
    logging.debug("loaded in %s",load_db)
    exec(load_db)
    return cfg
    
def connect(cfg_file, location):
    """I connect to a database"""
    my_cfg = get_conf(cfg_file)
    logging.debug("CONNECT %s, %s", cfg_file, location)
    conn_string = "user=%s password=%s host=%s port=%s dbname=%s"%(my_cfg[location]["username"],
                            my_cfg[location]["password"], 
                            my_cfg[location]["dbhost"], 
                            my_cfg[location]["dbport"],
                            my_cfg[location]["db"])
    logging.debug(conn_string)
    try:
        out = db.connect(conn_string)
    except Exception as excep:
        print(type(excep))
        print(excep)
        logging.error("Connection couldn't be established, check your config!")
        sys.exit(1)
    return out

def get_commands(a_file):
    """I read a file and return commands"""
    cmds = []
    with open(a_file, "r") as my_file:
        cmds = my_file.read()
    return cmds
    
def run(cmds, cfgFile, location):
    """I run sql on a database"""
    conn = connect(cfgFile, location)
    logging.debug("RUN %s, %s, %s",cmds, cfgFile, location)
    cur = conn.cursor()
    if type(cmds) not in (tuple, list):
        cmd = get_commands(cmds)
        cur.execute(cmd)
    else:
        cur.execute(cmds[0], tuple(cmds[1]))
        cmd = cmds[0]
    cmd_upper = cmd.upper()
    logging.debug("executed %s", cmd)
    if cmd_upper.startswith("SELECT"):
        logging.debug("Is a query")
        as_more = True
        out = []
        while (as_more):
            out_data = cur.fetchmany()
            if out_data == []:
                as_more = False
            else:
                out.append(out_data)
        logging.info("QUERY OUTPUT: %s", out)
    else:
        conn.commit()
        logging.debug("Commited")


def mk_object(objname="sbd", n=10, start_id=1, skip=0, tab="tab_oda_sbd"):
    '''I create objects'''
    call_it = None
    function_name = f"generate_test_{objname}()"
    print(function_name)
    sql = f"INSERT INTO {tab} VALUES (nextval('tab_oda_{objname}_id_seq'), %s, %s, %s, %s, %s, %s, %s)"
    out = []
    an_id = start_id
    for i in range(0, int(n)):
        now = datetime.datetime.now(datetime.timezone.utc)
        call_it = eval(function_name)
        new_obj = call_it.model_dump_json()
        in_data = [ new_obj,
                   "sbid", 
                   1,
                   "reptester",
                   now, 
                   now,
                   sys.argv[0]
                   ]
        out.append(in_data)
        an_id = an_id + skip + 1
    return [sql, out]

def main():
    """The main"""
    get_conf(sys.argv[1])
    #cmds = get_commands("init.sql")
    #print(cmds)
    data = mk_object()
    print(data)  

if __name__ == "__main__":
    main()
