"""
This script is for generating test SBDs
"""

import argparse
import datetime
import os
import random
import uuid
from pathlib import Path, PosixPath

from astropy.units import Quantity
from polyfactory.factories.pydantic_factory import ModelFactory
from ska_oso_pdm import Metadata, SBDefinition, Target
from ska_oso_pdm._shared.target import (
    EquatorialCoordinates,
    FivePointParameters,
    HorizontalCoordinates,
    PointingKind,
    PointingPattern,
    RadialVelocity,
    RadialVelocityReferenceFrame,
    RasterParameters,
    SinglePointParameters,
    SolarSystemObject,
    SolarSystemObjectName,
)
from ska_oso_pdm.sb_definition.sb_definition import SBD_SCHEMA_URI, TelescopeType

parser = argparse.ArgumentParser(description="Generate test SBDefinitions")
parser.add_argument(
    "--n",
    metavar="N",
    type=int,
    default=10,
    help="number of test files to be generated",
)
parser.add_argument(
    "--location",
    metavar="loc",
    type=str,
    default=None,
    help=(
        "full path to the directory where you want the test files to be stored, if none"
        " provided a directory will be generated immediately outside the project"
    ),
)


class SBDFactory(ModelFactory[SBDefinition]):
    ...


def create_directory():
    """ "
    create a directory in the root of the project to house the test data created each time this script  is run
    """
    project_root = Path(__file__).parents[2]
    directory_str = datetime.datetime.now().astimezone().strftime("%Y%m%d%H%M%S_%Z")

    dir_path = project_root / directory_str

    if dir_path.is_dir() is False:
        os.mkdir(dir_path)

    return dir_path


def create_target_list():
    """
    Polyfactory struggles with the complex data structure that we have for targets, this function generates a list
    of targets that we can use to randomly generate target lists for the SBDs.
    """
    target_1 = Target(
        target_id="target_1",
        pointing_pattern=PointingPattern(
            active=PointingKind.SINGLE_POINT,
            parameters=[
                SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=2.0)
            ],
        ),
        reference_coordinate=EquatorialCoordinates(ra=1.0, dec=-0.5),
        radial_velocity=RadialVelocity(
            quantity=Quantity(value=-12.345, unit="km / s"),
            reference_frame=RadialVelocityReferenceFrame.BARYCENTRIC,
        ),
    )

    target_2 = Target(
        target_id="target_2",
        pointing_pattern=PointingPattern(
            active=PointingKind.RASTER,
            parameters=[
                RasterParameters(
                    row_length_arcsec=1.0,
                    row_offset_arcsec=1.0,
                    n_rows=2,
                    pa=1.0,
                    unidirectional=True,
                ),
                SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=2.0),
            ],
            reference_coordinate=HorizontalCoordinates(
                az=1, el=0.5, unit=("deg", "deg")
            ),
        ),
    )

    target_3 = Target(
        target_id="target_3",
        pointing_pattern=PointingPattern(
            active=PointingKind.FIVE_POINT,
            parameters=[
                FivePointParameters(offset_arcsec=5.0),
                RasterParameters(
                    row_length_arcsec=1.0,
                    row_offset_arcsec=1.0,
                    n_rows=2,
                    pa=1.0,
                    unidirectional=True,
                ),
                SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=2.0),
            ],
            reference_coordinate=EquatorialCoordinates(ra=13.6, dec=-72.0),
        ),
    )

    target_4 = Target(
        target_id="planet",
        pointing_pattern=PointingPattern(
            active=PointingKind.SINGLE_POINT,
            parameters=[
                FivePointParameters(offset_arcsec=5.0),
                RasterParameters(
                    row_length_arcsec=1.0,
                    row_offset_arcsec=1.0,
                    n_rows=2,
                    pa=1.0,
                    unidirectional=True,
                ),
                SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=2.0),
            ],
        ),
        reference_coordinate=SolarSystemObject(name=SolarSystemObjectName.VENUS),
    )

    return target_1, target_2, target_3, target_4


def generate_random_target_list(target_list):
    """
    function that creates a set of random targets from a list
    """

    return random.sample(target_list, random.randint(1, len(target_list)))


def generate_test_sbd():
    """
    generates a test SBD using polyfactory - polyfactory struggles to generate valid SBDefinitions so this function
    just keeps trying until it hits gold, which is pretty nasty.
    """
    sbd = None
    while sbd is None:
        try:
            sbd = SBDFactory.build(
                interface=SBD_SCHEMA_URI, metadata=Metadata(), targets=[], telescope=random.choice(list(TelescopeType))
            )
        except Exception:
            pass

    if sbd.telescope is TelescopeType.SKA_MID or TelescopeType.MEERKAT:
        sbd.mccs_allocation = None
    else:
        sbd.dish_allocations = None
        sbd.dish_configurations = None
    return sbd

def main():
    args = parser.parse_args()
    number_of_files = args.n

    if args.location is not None:
        dir_path = PosixPath(args.location)
        if dir_path.is_dir() is False:
            os.mkdir(dir_path)
    else:
        dir_path = create_directory()

    targets = create_target_list()

    file_counter = 0
    while file_counter < number_of_files:
        sbd = generate_test_sbd()
        sbd.targets = generate_random_target_list(targets)
        file_name = str(uuid.uuid1()).split("-")[0] + ".json"
        with open(dir_path / file_name, "w") as fh:
            fh.write(sbd.model_dump_json())

        file_counter += 1


if __name__ == "__main__":
    main()

