#!/bin/sh

echo """ALTER TABLE $1 SET (timescaledb.compress, timescaledb.compress_segmentby = '$2');
SELECT add_compression_policy('$1', INTERVAL '7 days');"""
