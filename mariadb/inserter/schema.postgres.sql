CREATE TABLE testtable (
    id SERIAL,
    comment VARCHAR(200),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
