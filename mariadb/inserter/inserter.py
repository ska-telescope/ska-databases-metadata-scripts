'''I insert data'''
from cfg import database, connection
import time
from faker import Faker

fake = Faker()

if database == "mysql":
    from mysql.connector import Error
else:
    from psycopg2 import Error

def create_connection():
    '''Function to connect to the DB'''
    try:
        if database == "mysql":
            if connection.is_connected():
                print(f"Connected to a {database} database")
            return connection
        else:
            print(f"Connected to a {database} database")
            return connection
    except Error as e:
        print(f"Error: {e}")
        return None

def insert_data(conn, wait_time=1):
    '''Function to insert data into the table'''
    try:
        cursor = conn.cursor()
        # Define the query to insert into the table
        insert_query = """
        INSERT INTO testtable (comment, created_at)
        VALUES (%s, %s)
        """
        
        while True:
            # Generate random comment and current timestamp
            comment = fake.text()
            created_at = time.strftime('%Y-%m-%d %H:%M:%S')

            # Execute the insertion
            cursor.execute(insert_query, (comment, created_at))

            connection.commit()
            print(f"Inserted: {comment} at {created_at}")
            time.sleep(wait_time)

    except Error as e:
        print(f"Error while inserting data: {e}")
    finally:
        if cursor:
            cursor.close()

def main():
    '''The main'''
    conn = create_connection()

    if conn:
        try:
            # Call insert data in an infinite loop
            insert_data(conn)
        finally:
            conn.close()
            print("Connection closed.")

if __name__ == "__main__":
    main()