# Inserter container
This container will insert data on a mariadb. This is usefull to test mariadb or operator upgrades. 

# Usage
Create a database, an user for that database and load the schema.mariadb.sql on this new database

On the pod, configure these variables or use a dotenv file with:
  * DB_TYPE: mysql or postgres
  * DB_HOST: the primary database ip
  * DB_USER: the username
  * DB_PASSWORD: the user's password
  * DB_NAME: the database name
  * DB_SCHEMA: the schema name - optional

Once up and running it will create one entry each second on the database.testable.
