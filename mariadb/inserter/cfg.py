'''I read variables and get a connection'''
import os
from dotenv import load_dotenv

connection = None


def load_config():
    '''I read config values'''
    load_dotenv()
    return {
        "DB_TYPE": os.getenv("DB_TYPE"),
        "DB_HOST": os.getenv("DB_HOST"),
        "DB_PORT": os.getenv("DB_PORT"),
        "DB_NAME": os.getenv("DB_NAME"),
        "DB_SCHEMA": os.getenv("DB_SCHEMA"),
        "DB_USER": os.getenv("DB_USER"),
        "DB_PASSWORD": os.getenv("DB_PASSWORD")
    }


config = load_config()
database = os.getenv("DB_TYPE")

if config["DB_TYPE"] in ["mysql", "mariadb"]:
    import mysql.connector
    connection = mysql.connector.connect(
        host=config['DB_HOST'],
        user=config['DB_USER'],
        password=config['DB_PASSWORD'],
        database=config['DB_NAME']
    )
else:
    import psycopg2
    if config["DB_SCHEMA"] == None:
        connection = psycopg2.connect(
            host=config['DB_HOST'],
            user=config['DB_USER'],
            password=config['DB_PASSWORD'],
            database=config['DB_NAME'],
            port=config['DB_PORT']
        )
    else:
        opts = f"-c search_path={config['DB_SCHEMA']}"
        connection = psycopg2.connect(
            host=config['DB_HOST'],
            user=config['DB_USER'],
            password=config['DB_PASSWORD'],
            database=config['DB_NAME'],
            port=config['DB_PORT'],
            options=opts
        )
