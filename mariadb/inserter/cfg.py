'''I read variables and get a connection'''
import os
from dotenv import load_dotenv


connection = None

def load_config():
    '''I read config values'''
    load_dotenv()
    return {
        "DB_TYPE": os.getenv("DB_TYPE"),
        "DB_HOST": os.getenv("DB_HOST"),
        "DB_PORT": os.getenv("DB_PORT"),
        "DB_NAME": os.getenv("DB_NAME"),
        "DB_USER": os.getenv("DB_USER"),
        "DB_PASSWORD": os.getenv("DB_PASSWORD")
    }

config = load_config()

if config["DB_TYPE"] in [ "mysql", "mariadb"]:
    import mysql.connector
    connection = mysql.connector.connect(
            host=config["DB_HOST"],
            user=config['DB_USER'],
            password=config['DB_PASSWORD'],
            database=config['DB_NAME']
        )
else:
    import psycopg2
    connection = psycopg2.connect(
            host=config["DB_HOST"],
            user=config['DB_USER'],
            password=config['DB_PASSWORD'],
            database=config['DB_NAME'],
            port=config["DB_PORT"]
    )